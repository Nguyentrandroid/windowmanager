package com.example.nguyentrandroid.aaaaaaaaa;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

public class FloatingService extends Service {
    private WindowManager windowManager;
    private MyGroupView mViewIcon;
    private WindowManager.LayoutParams params;
    ImageView imageView;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initView();
        return START_STICKY;
    }

    private void initView() {
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        createViewIcon();
        showIcon();
    }

    private void showIcon() {
        windowManager.addView(mViewIcon, params);
    }

    private void createViewIcon() {
        mViewIcon = new MyGroupView(this);
        View view = View.inflate(this, R.layout.view_icon, mViewIcon);
        ImageView img = (ImageView) view.findViewById(R.id.img);
        params = new WindowManager.LayoutParams();
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;
        params.format = PixelFormat.TRANSLUCENT;
        params.type = WindowManager.LayoutParams.TYPE_PHONE;
        params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        params.flags |= WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
    }
}
